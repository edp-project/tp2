#!/usr/env/bin python2
#Copyright (c) 2016, Payet Thibault
#All rights reserved.
#
#Redistribution and use in source and binary forms, with or without
#modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of the Monwarez Inc nor the
#      names of its contributors may be used to endorse or promote products
#      derived from this software without specific prior written permission.
#
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
#ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#DISCLAIMED. IN NO EVENT SHALL PAYET THIBAULT BE LIABLE FOR ANY
#DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
#(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
#ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
#SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
import math
import sys
import numpy as np
import matplotlib.pyplot as plt
class Advection:
    def __init__(self, c, J , T):
        self._a         = 1
        self._c         = c
        self._T         = T
        self._J         = J
        self._deltaX    = 1.0 / self._J
        self._deltaT    = self._c * self._deltaX / math.fabs(self._a)
        self._u         = np.zeros(self._J)
        self._uG        = np.zeros(self._J + 1)
        self._ratio     = self._a * (self._deltaT / self._deltaX)

    def _g(self,x):
        return np.sin(2*math.pi*x)
    def _BoundU(self):
        for j in range (0,self._J):
            self._u[j]    =   self._g((j - 0.5)*self._deltaX)

    def _BoundUG(self):
        self._uG[0]    =    self._g(0)

    def _BoundCondition (self):
        self._BoundU()
        self._BoundUG()

    def _uLoop(self):
        for j in range (0, self._J):
            self._u[j]   = self._u[j] - self._ratio*(self._uG[j+1] - self._uG[j])

    def Run(self):
        self._BoundCondition()
        t    =    0
        X    =    np.linspace(0,1,self._J,endpoint=True)
        niter=    0

        while t < self._T :
           self._uGLoop()
           self._uLoop()
           t += self._deltaT
           niter = niter + 1

           if niter%10 == 0:
               plt.hold(False)
               plt.plot(X,self._u,"b:.")
               plt.grid()
               plt.pause(self._deltaT)
               plt.hold(True)
               print("iteration n = ", niter, "temps t = ",t)

        print("End of iteration")
        plt.plot(X,self._u)
        plt.show()

